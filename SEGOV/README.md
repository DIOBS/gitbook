# Levantamento de Indicadores Socioeconômicos e Estudo da Evolução Urbana de Bairros de Fortaleza

## 1. Introdução

Este relatório apresenta o diagnóstico de nove bairros do município de Fortaleza, com o objetivo de subsidiar a implementação de ações e projetos da Secretaria Municipal de Governo (SEGOV). Neste relatório, são fornecidos parâmetros relacionados à evolução urbana de Fortaleza e indicadores socioeconômicos para os bairros presentes na Tabela 01.&#x20;

Este diagnóstico está subdivido em sete seções: (i) introdução, em que apresentamos a localização e listagem dos bairros que compõem o estudo; (ii) ocupação e evolução urbana; (iii) indicadores socioeconômicos; (iv) meio ambiente; (v) equipamentos públicos; (vi) desafios e potencialidades e (vii) conclusão. Ao final, anexos trazem dos dados brutos e processados analisados.

**Tabela 01:** Bairros Analisados

| Centro            |
| ----------------- |
| Praia de Iracema  |
| Meireles          |
| Mucuripe          |
| Varjota           |
| Cais do Porto     |
| Vicente Pinzón    |
| Papicu            |
| Praia do Futuro I |

{% embed url="https://datawrapper.dwcdn.net/oBOGn/1/" %}
