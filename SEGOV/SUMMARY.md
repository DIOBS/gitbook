# Table of contents

* [Levantamento de Indicadores Socioeconômicos e Estudo da Evolução Urbana de Bairros de Fortaleza](README.md)

## 2. Ocupação e Evolução Urbana

* [2.1. Contexto Histórico e Expansão Territorial](2.-ocupacao-e-evolucao-urbana/2.1.-contexto-historico-e-expansao-territorial.md)
* [2.2. Evolução da Frota de Veículos Automotores](2.-ocupacao-e-evolucao-urbana/2.2.-evolucao-da-frota-de-veiculos-automotores.md)

## 3. Indicadores Socioeconômicos

* [3.1. População e Urbanização](3.-indicadores-socioeconomicos/3.1.-populacao-e-urbanizacao.md)
* [3.2. Emprego e Renda](3.-indicadores-socioeconomicos/3.2.-emprego-e-renda.md)
* [3.3. Atividades Econômicas](3.-indicadores-socioeconomicos/3.3.-atividades-economicas.md)
* [3.4. Taxa de Analfabetismo](3.-indicadores-socioeconomicos/3.4.-taxa-de-analfabetismo.md)

## 4. Meio Ambiente

* [4.1. Tipos Climáticos](4.-meio-ambiente/4.1.-tipos-climaticos.md)

## 5. Equipamentos Públicos

* [5.1. Cultura](5.-equipamentos-publicos/5.1.-cultura.md)
* [5.2. Educação](5.-equipamentos-publicos/5.2.-educacao.md)
* [5.3. Saúde](5.-equipamentos-publicos/5.3.-saude.md)

## 6. Desafios e Potencialidades

* [\<em construção>](6.-desafios-e-potencialidades/less-than-em-construcao-greater-than.md)

## 7. Conclusão

* [x.1. Considerações Finais](7.-conclusao/x.1.-consideracoes-finais.md)
